const express = require('express');
const mongoose = require('mongoose');
const requireDir = require ('require-dir');
const cors = require ('cors');

//iniciando  o app
const app = express();

//informando q vai usar formato json
app.use(express.json());
//permitir que outros sistemas acessem essa api
app.use(cors());

//iniciando conexão com banco
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/bancoapinode", { useMongoClient: true }).then(() => {
  console.log("MongoDB conectado...");
}).catch((err) => {
  console.log('Houve um erro ao se conectar ao banco: ' + err);
});

//permissão de todos os models, para q possam ser usados
requireDir('./src/models');

/*difinindo q a rota iniciará com 'api' e dps joga para o arquivo de rotas, para pegar o 
resto da url e executar um em específico*/
app.use('/api', require('./src/routes'));


//definindo qual porta que usará
app.listen(3000);