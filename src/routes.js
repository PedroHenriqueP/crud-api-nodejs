const express = require ('express');
const routes = express.Router();
const ProductController = require('./controller/ProductController');

//pega o resto da url e redireciona pra parte do controller específico da classe
routes.get('/products', ProductController.index);
routes.get('/products/:id', ProductController.show);
routes.post('/products', ProductController.store);
routes.put('/products/:id', ProductController.update);
routes.delete('/products/:id', ProductController.destroy);

//exportar essa classe para q possa ser usada em outro arquivo
module.exports = routes;